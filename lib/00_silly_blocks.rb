def reverser
  yield.split.each {|word| word.reverse!}.join(' ')
end

def adder(num=1)
  yield + num
end

def repeater(num=1)
  idx = 0
  while idx < num
    yield
    idx += 1
  end
end

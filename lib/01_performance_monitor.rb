def measure(n=1)
  start = Time.now
  n.times { yield }
  end_time = Time.now
  elapsed_time = end_time - start
  average_time = elapsed_time / n
end
